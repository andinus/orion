module framagit.org/andinus/orion

go 1.13

require (
	github.com/AlecAivazis/survey/v2 v2.0.7
	github.com/briandowns/spinner v1.9.0
	github.com/fatih/color v1.7.0
)
